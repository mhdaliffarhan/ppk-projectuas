package com.example.academy.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.academy.model.CourseModal;

import java.util.ArrayList;

public class DBHandler extends SQLiteOpenHelper {
    private static final String DB_NAME = "academy";
    private static final int DB_VERSION = 1;
    private static final String TABLE_NAME = "course";
    private static final String ID_COL = "id";
    private static final String NAME_COL = "name";
    private static final String TYPE_COL = "type";
    private static final String PRICE_COL = "price";
    private static final String DESCRIPTION_COL = "description";

    public DBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    public DBHandler(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAME_COL + " TEXT,"
                + TYPE_COL + " TEXT,"
                + PRICE_COL + " TEXT,"
                + DESCRIPTION_COL + " TEXT)";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addNewCourse(String name, String type, String price, String desc){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NAME_COL, name);
        values.put(TYPE_COL, type);
        values.put(PRICE_COL, price);
        values.put(DESCRIPTION_COL, desc);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public ArrayList<CourseModal> readCourses() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);

        ArrayList<CourseModal> courseModalArrayList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                courseModalArrayList.add(new CourseModal(cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return courseModalArrayList;
    }

}
