package com.example.academy.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.academy.R;
import com.example.academy.model.CourseModal;

import java.util.ArrayList;

public class CourseRVAdapter extends RecyclerView.Adapter<CourseRVAdapter.ViewHolder> {

    private ArrayList<CourseModal> courseModalArrayList;
    private Context context;

    public CourseRVAdapter(ArrayList<CourseModal> courseModalArrayList, Context context) {
        this.courseModalArrayList = courseModalArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.course_rv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CourseModal modal = courseModalArrayList.get(position);
        holder.nameTV.setText(modal.getName());
        holder.typeTV.setText(modal.getType());
        holder.priceTV.setText(modal.getPrice());
        holder.descTV.setText(modal.getDesc());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(context, UpdateCourseActivity.class);
//
//                i.putExtra("name", modal.getName());
//                i.putExtra("type", modal.getType());
//                i.putExtra("price", modal.getPrice());
//                i.putExtra("desc", modal.getDesc());
//
//                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return courseModalArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTV, typeTV, priceTV, descTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.nameTextView);
            typeTV = itemView.findViewById(R.id.typeTextView);
            priceTV = itemView.findViewById(R.id.priceTextView);
            descTV = itemView.findViewById(R.id.descTextView);
        }
    }
}


