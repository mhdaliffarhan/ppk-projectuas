package com.example.academy;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.academy.ui.dashboard.TambahCourseActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    // Deklarasi variabel yang dibutuhkan
    private EditText mEmailField;
    private EditText mPasswordField;
    private Button mLoginButton, mRegisterButton, mTambahCourse;

    // Mendapatkan instance FirebaseAuth
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inisialisasi variabel yang dideklarasikan sebelumnya
        mEmailField = findViewById(R.id.email);
        mPasswordField = findViewById(R.id.password);
        mLoginButton = findViewById(R.id.login);
        mRegisterButton = findViewById(R.id.toregister);
        mTambahCourse = findViewById(R.id.tambahCourse);

        mAuth = FirebaseAuth.getInstance();

        // Menambahkan listener pada tombol login
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mEmailField.getText().toString();
                String password = mPasswordField.getText().toString();

                // Validasi input email
                if (email.isEmpty()) {
                    mEmailField.setError("Email tidak boleh kosong");
                    return;
                }

                // Validasi input password
                if (password.isEmpty()) {
                    mPasswordField.setError("Password tidak boleh kosong");
                    return;
                }

                if (password.length() < 8) {
                    mPasswordField.setError("Password harus minimal 8 karakter");
                    return;
                }

                // Memanggil fungsi signIn dengan parameter email dan password
                signIn(email, password);
            }
        });

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });

        mTambahCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TambahCourseActivity.class);
                startActivity(intent);
            }
        });

    }

    // Fungsi untuk melakukan autentikasi login
    private void signIn(String email, String password) {
        // Mencoba melakukan login dengan email dan password yang diberikan
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Jika login berhasil, tampilkan pesan toast dan arahkan ke halaman yang sesuai
                    Toast.makeText(MainActivity.this, "Login berhasil", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intent);
                } else {
                    // Jika login gagal, tampilkan pesan toast
                    Toast.makeText(MainActivity.this, "Email atau Passowrd salah, coba lagi!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    return;
                }
            }
        });
    }
}