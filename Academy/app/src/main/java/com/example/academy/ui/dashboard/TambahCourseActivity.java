package com.example.academy.ui.dashboard;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.academy.MainActivity;
import com.example.academy.R;
import com.example.academy.database.DBHandler;

public class TambahCourseActivity extends AppCompatActivity {
    private EditText nameET, typeET, priceET, descET;
    private Button saveBtn;
    private DBHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_course);

            nameET = findViewById(R.id.nameEditText);
            typeET = findViewById(R.id.typeEditText);
            priceET = findViewById(R.id.priceEditText);
            descET = findViewById(R.id.descEditText);
            saveBtn = findViewById(R.id.tambahButton);
            dbHandler = new DBHandler(this);

            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String name = nameET.getText().toString();
                    String type = typeET.getText().toString();
                    String price = priceET.getText().toString();
                    String desc = descET.getText().toString();

                    if (name.isEmpty() || type.isEmpty() || price.isEmpty() || desc.isEmpty()) {
                        Toast.makeText(TambahCourseActivity.this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
                    } else {
                        dbHandler.addNewCourse(name, type, price, desc);
                        nameET.setText("");
                        typeET.setText("");
                        priceET.setText("");
                        descET.setText("");
                        Toast.makeText(TambahCourseActivity.this, "Course added successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(TambahCourseActivity.this, MainActivity.class);
                    }
                }
            });
        }

}