package com.example.academy;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.academy.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegistrationActivity extends AppCompatActivity {

    // Deklarasi variabel yang dibutuhkan
    private EditText mFullnameField;
    private EditText mEmailField;
    private EditText mUsernameField;
    private EditText mPasswordField;
    private EditText mCpasswordField;
    private Button mRegisterButton;

    // Mendapatkan instance FirebaseAuth
    private FirebaseAuth mAuth;
    // Mendapatkan reference ke database
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // Inisialisasi variabel yang dideklarasikan sebelumnya
        mFullnameField = findViewById(R.id.fullname);
        mEmailField = findViewById(R.id.email);
        mUsernameField = findViewById(R.id.username);
        mPasswordField = findViewById(R.id.password);
        mCpasswordField = findViewById(R.id.cpassword);
        mRegisterButton = findViewById(R.id.register);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Menambahkan listener pada tombol register
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullname = mFullnameField.getText().toString();
                String email = mEmailField.getText().toString();
                String username = mUsernameField.getText().toString();
                String password = mPasswordField.getText().toString();
                String cPassword = mCpasswordField.getText().toString();

                // Validasi input
                if (fullname.isEmpty()) {
                    mFullnameField.setError("Nama lengkap harus diisi");
                    return;
                }
                if (email.isEmpty()) {
                    mEmailField.setError("Email harus diisi");
                    return;
                }
                if (username.isEmpty()) {
                    mUsernameField.setError("Username harus diisi");
                    return;
                }
                if (password.isEmpty()) {
                    mPasswordField.setError("Password harus diisi");
                    return;
                }
                if (cPassword.isEmpty()) {
                    mCpasswordField.setError("Konfirmasi password harus diisi");
                    return;
                }
                if (!password.equals(cPassword)) {
                    mCpasswordField.setError("Konfirmasi password tidak sesuai");
                    return;
                }
                // Validate email format
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mEmailField.setError("Format email tidak valid");
                    return;
                }

                // Validate password length
                if (password.length() < 8) {
                    mPasswordField.setError("Password minimal 8 karakter");
                    return;
                }

                // Check if email is already registered in Firebase Realtime Database
                mDatabase.orderByChild("email").equalTo(email).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            mEmailField.setError("Email sudah terdaftar");
                        } else {
                            register(fullname, email, username, password);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) { }
                });
            }
        });
    }

    // Fungsi untuk melakukan registrasi
    private void register(final String fullname, final String email, final String username, String password) {
        // Mencoba mendaftarkan pengguna baru dengan email dan password yang diberikan
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Jika registrasi berhasil, tampilkan pesan toast
                            Toast.makeText(RegistrationActivity.this, "Registrasi berhasil", Toast.LENGTH_SHORT).show();
                            // Dapatkan id pengguna yang baru saja mendaftar
                            String userId = mAuth.getCurrentUser().getUid();

                            // Buat object User dengan informasi yang diberikan
                            User user = new User(fullname, email, username);

                            // Tambahkan data ke database dengan menggunakan id pengguna sebagai key
                            mDatabase.child("users").child(userId).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    // Jika proses menambahkan data ke database selesai, arahkan ke halaman yang sesuai
                                    Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                            });
                        } else {
                            // Jika registrasi gagal, tampilkan pesan toast
                            Toast.makeText(RegistrationActivity.this, "Registrasi gagal, silakan coba lagi", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}