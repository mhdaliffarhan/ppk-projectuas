package com.example.academy.ui.notifications;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.academy.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class NotificationsFragment extends Fragment {

    // Deklarasi variabel
    private EditText mFullnameField;
    private TextView mEmailField;
    private EditText mUsernameField;
    private EditText mNewPasswordField;
    private EditText mOldPassowrdField;
    private Button mSaveChangeButton;
    private Button mChangePasswordButton;

    // Context dari fragment ini
    private Context mContext;

    // Mendapatkan instance FirebaseAuth
    private FirebaseAuth mAuth;

    // Mendapatkan reference ke database
    private DatabaseReference mDatabase;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        // Inisialisasi variabel yang dideklarasikan sebelumnya
        mFullnameField = root.findViewById(R.id.input_namalengkap);
        mEmailField = root.findViewById(R.id.input_email);
        mUsernameField = root.findViewById(R.id.input_username);
        mSaveChangeButton = root.findViewById(R.id.btnSaveChange);
        mChangePasswordButton = root.findViewById(R.id.btn_change_password);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Mengambil data user dari database
        mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Mengisi EditText dengan data dari database
                mFullnameField.setText(dataSnapshot.child("fullname").getValue().toString());
                mEmailField.setText(dataSnapshot.child("email").getValue().toString());
                mUsernameField.setText(dataSnapshot.child("username").getValue().toString());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Menampilkan pesan error jika terjadi kesalahan saat mengambil data dari database
                Toast.makeText(mContext, databaseError.getMessage(), Toast.LENGTH_SHORT).show();}
        });

        // Mengubah data username dan nama lengkap
        mSaveChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Mengambil input dari user
                final String fullname = mFullnameField.getText().toString().trim();
                final String username = mUsernameField.getText().toString().trim();

                // Validasi input
                if (fullname.isEmpty()) {
                    mFullnameField.setError("Nama lengkap harus diisi");
                    return;
                }
                if (username.isEmpty()) {
                    mUsernameField.setError("Username harus diisi");
                    return;
                }

                // Memperbaharui data user di database
                mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child("fullname").setValue(fullname);
                mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).child("username").setValue(username);

                // Menampilkan toast message untuk menandakan bahwa proses pengubahan berhasil
                Toast.makeText(mContext, "Pengubahan data profil berhasil", Toast.LENGTH_SHORT).show();
            }
        });

        // Mengubah Password
        mChangePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Ambil input dari EditText
                final String oldPassword = mOldPassowrdField.getText().toString().trim();
                final String newPassword = mNewPasswordField.getText().toString().trim();

                // Validasi input
                if (oldPassword.isEmpty()) {
                    mOldPassowrdField.setError("Password lama harus diisi");
                    return;
                }
                if (newPassword.isEmpty()) {
                    mNewPasswordField.setError("Password baru harus diisi");
                    return;
                }

                // Dapatkan user yang sedang login
                final FirebaseUser user = mAuth.getCurrentUser();

                // Membuat objek AuthCredential yang diperlukan untuk mengubah password
                AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), oldPassword);

                // Mencoba mengubah password dengan menggunakan objek AuthCredential yang telah dibuat
                user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // Jika proses reauthentication berhasil, maka password dapat diubah
                            user.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        // Jika proses pengubahan password berhasil, maka tampilkan toast message
                                        Toast.makeText(mContext, "Password berhasil diubah", Toast.LENGTH_SHORT).show();
                                    } else {
                                        // Jika terjadi kesalahan saat mengubah password, maka tampilkan pesan error
                                        Toast.makeText(mContext, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            // Jika proses reauthentication gagal, maka tampilkan pesan error
                            Toast.makeText(mContext, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        return root;
    }
}